var Persona = require('./modelo/persona');
var Controller = require('./controller');

module.exports=function(app){

	app.get('/persona',Controller.getPersona);
	app.post('/persona',Controller.setPersona);
	app.put('/persona/:persona_id',Controller.updatePersona);
	app.delete('/persona/:persona_id',Controller.removePersona);

	app.get('*',function(req,resp){
			resp.sendfile('./angular/index.html');
	});
}