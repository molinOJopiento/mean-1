angular.module('MainApp', ["ngTable"])

function mainController($scope, $http, NgTableParams) {
	$scope.newPersona = {};
	$scope.personas = {};
	$scope.selected = false;

	//PAGINACION

	// Obtenemos todos los datos de la base de datos
	$http.get('/persona').success(function(data) {
		$scope.personas = data;
		$scope.hola = data;
		self.tableParams = new NgTableParams({}, { dataset: $scope.personas});

		$scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    	}
	})
	.error(function(data) {
		console.log('Error: ' + data);
	});

	//NgTable


	// Función para registrar a una persona
	$scope.registrarPersona = function() {
		$http.post('/persona', $scope.newPersona)
		.success(function(data) {
				$scope.newPersona = {}; // Borramos los datos del formulario
				$scope.personas = data;
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función para editar los datos de una persona
	$scope.modificarPersona = function(newPersona) {
		$http.put('/persona/' + $scope.newPersona._id, $scope.newPersona)
		.success(function(data) {
				$scope.newPersona = {}; // Borramos los datos del formulario
				$scope.personas = data;
				$scope.selected = false;
			})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función que borra un objeto persona conocido su id
	$scope.borrarPersona = function(newPersona) {
		$http.delete('/persona/' + $scope.newPersona._id)
		.success(function(data) {
			$scope.newPersona = {};
			$scope.personas = data;
			$scope.selected = false;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	// Función para coger el objeto seleccionado en la tabla
	$scope.selectPerson = function(persona) {
		$scope.newPersona = persona;
		$scope.selected = true;
		console.log($scope.newPersona, $scope.selected);
	};
}